# Project #1
### Biography/Personal Site

##### Concepts:
* HTML
* CSS
* Other
  * Block Element Modifier (BEM)

##### Notes:
[Margin collapsing](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Box_Model/Mastering_margin_collapsing) happens on the h1 and h3. This means that the h1 element's bottom margin beats out the h3's top margin. This ignores the h3's margin completely.

[rem vs em](http://zellwk.com/blog/rem-vs-em/) is a good read to understand when to use rem or em. To keep it simple, introduce pixel sizing first and transition them to rem/em later on.
